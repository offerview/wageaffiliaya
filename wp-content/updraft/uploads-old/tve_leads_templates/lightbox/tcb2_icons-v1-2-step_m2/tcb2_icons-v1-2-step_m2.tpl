<div class="thrv_wrapper thrv_icon tcb-icon-display tve_evt_manager_listen tve_et_click tve_ea_thrive_leads_form_close" data-css="tve-u-05b4a18534e69e" data-tcb-events="__TCB_EVENT_[{&quot;a&quot;:&quot;thrive_leads_form_close&quot;,&quot;t&quot;:&quot;click&quot;}]_TNEVE_BCT__">
<svg class="tcb-icon" viewBox="0 0 30 32" data-name="close">
<path d="M0.655 2.801l1.257-1.257 27.655 27.655-1.257 1.257-27.655-27.655z"></path>
<path d="M28.31 1.543l1.257 1.257-27.655 27.655-1.257-1.257 27.655-27.655z"></path>
</svg>
</div>
<div class="thrv_wrapper thrv-page-section" style="" data-css="tve-u-15b4a18534e6e8">
<div class="tve-page-section-out" data-css="tve-u-25b4a18534e72e"></div>
<div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-35b4a18534e779"><div class="thrv_wrapper thrv_heading" style="" data-css="tve-u-45b4a18534e7b8" data-tag="h1"><h1 data-css="tve-u-55b4a18534e7f5" style="text-align: center;"><span data-css="tve-u-65b4a18534e859" style="font-weight: 300; font-family: Poppins;"><strong>Sign Up Below</strong>... <em></em>and Get Instant Access to the <strong>Freebie</strong></span></h1></div><div class="thrv_wrapper thrv-columns" style=""><div class="tcb-flex-row tcb-resized tcb--cols--2" data-css="tve-u-75b4a18534e86b"><div class="tcb-flex-col" data-css="tve-u-85b4a18534e8c0" style=""><div class="tcb-col tve_empty_dropzone"><div class="thrv_wrapper tve_image_caption tcb-mobile-hidden" data-css="tve-u-95b4a18534e907" style=""><span class="tve_image_frame" style="width: 100%;"><img class="tve_image wp-image-6329" alt="" width="512" height="512" title="download_icon" data-id="6329" src="{tve_leads_base_url}/images/download_icon.png" style="width: 100%;"></span></div></div></div><div class="tcb-flex-col" data-css="tve-u-105b4a18534e94c" style=""><div class="tcb-col tve_empty_dropzone" style="" data-css="tve-u-115b4a18534e991"><div class="thrv_wrapper thrv_lead_generation" data-connection="api" style=""><input type="hidden" class="tve-lg-err-msg" value="{&quot;email&quot;:&quot;Email address invalid&quot;,&quot;phone&quot;:&quot;Phone number invalid&quot;,&quot;password&quot;:&quot;Password invalid&quot;,&quot;passwordmismatch&quot;:&quot;Password mismatch error&quot;,&quot;required&quot;:&quot;Required field missing&quot;}">
<div class="thrv_lead_generation_container tve_clearfix">
<form action="#" method="post" novalidate="novalidate">
<div class="tve_lead_generated_inputs_container tve_clearfix tve_empty_dropzone">
<div class="tve_lg_input_container tve_lg_input" data-css="tve-u-125b4a18534e9f3">
<input type="text" data-field="name" name="name" placeholder="Name..." data-placeholder="Name..." style="">
</div>
<div class="tve_lg_input_container tve_lg_input" data-css="tve-u-135b4a18534ea32">
<input type="email" data-field="email" data-required="1" data-validation="email" name="email" placeholder="Email..." data-placeholder="Email..." style="">
</div>
<div class="tve_lg_input_container tve_submit_container tve_lg_submit" data-css="tve-u-145b4a18534ea70" data-tcb_hover_state_parent="false">
<button type="submit" class="tve-froala" style="">Get it Now</button>
</div>
</div>
</form>
</div>
</div></div></div></div></div></div>
</div>